<?php

namespace App\Http\Controllers;

use App\Jobs\CheckReservationExpirationDate;
use App\Reservation;
use App\Room;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        CheckReservationExpirationDate::dispatchNow();

        $rooms = Room::with('reservations')->get();
        $sortedRooms = DB::select("SELECT DISTINCT * FROM `reservations` where beginning_date IN (SELECT min(beginning_date) from `reservations` GROUP BY room_id)
                            ORDER BY room_id");
        if (count($rooms) > 0) return $this->setStatusCode(200)->respond(['rooms' => $rooms, 'sortedRooms' => $sortedRooms]);
//        return response()->setStatusCode(404)->respond(['rooms' => []]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'roomName' => 'required|max:255',
            'roomPic' => 'image|mimes:jpeg,png,jpg,gif,svg'//|max:2048',
        ]);
        if ($request->input('roomPic')) {
            $imageData = $request->input('roomPic');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->input('roomPic'))->save('images/' . $fileName);
            $room = Room::create([
                'name' => $request->input('roomName'),
                'room_pic' => '/images/' . $fileName
            ]);
            return $this->setStatusCode(200)->respond(['message' => 'Успешно добавлено', 'room' => $room]);
        }
        $room = Room::create([
            'name' => $request->input('roomName'),
            'room_pic' => '/images/default.png'
        ]);
        return $this->setStatusCode(200)->respond(['message' => 'Успешно добавлено', 'room' => $room]);



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Room $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('room_pic') == ''){
            $room = Room::find($id);
            $room->update([
                'name' => $request->input('name')
            ]);
           return $this->setStatusCode(200)->respond(['message' => 'Обновлен и новое фото не добавлено']);
        }
        $room = Room::find($id);

        $imageData = $request->input('room_pic');

        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
//        dd($imageData);

        Image::make($imageData)->save('images/'.$fileName);
        $room->update([
            'name' => $request->input('name'),
            'room_pic' => '/images/'.$fileName
        ]);
        $updatedRoom = Room::find($id);
        return $this->setStatusCode(200)->respond(['message' => 'Обновлен', 'updatedRoom' => $updatedRoom]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);
        $room->delete();
    }
    public function showReservationsOfRoom($id)
    {
//        $room = Room::with('reservations')->find($id);
        return view('showReservationsOfRoom', ['room_id' => $id]);
    }
    public function showUpdatedReservationsOfRoom($id)
    {
        return $this->setStatusCode(200)->respond(['room' => Room::with('reservations')->find($id)]);
    }
}
