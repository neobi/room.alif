<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Room;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'who_reserved_the_room' => 'required|max:255',
            'description' => 'required|max:255',
            'beginning_date' => 'required|date',
            'ending_date' => 'required|date'
        ]);

        $room_id = $request->input('room_id');
        $reserver = $request->input('who_reserved_the_room');
        $description = $request->input('description');
        $beg_date_taken_from_user = Carbon::create($request->input('beginning_date'))->format('Y-m-d H:i:s');
        $end_date_taken_from_user = Carbon::create($request->input('ending_date'))->format('Y-m-d H:i:s');


        $now = Carbon::now();

        if ($beg_date_taken_from_user < $now) return $this->setStatusCode(400)->respond(['message' => 'Начинающая дата уже прошла']);

        $room = Room::with('reservations')->find($room_id);

        if ($room->reservations) {
            foreach ($room->reservations as $reservation) {

                $beg_date1 = Carbon::create($reservation->beginning_date)->format('Y-m-d H:i:s');
                $end_date1 = Carbon::create($reservation->ending_date)->format('Y-m-d H:i:s');

                if ($end_date1 > $now) {
                    if ($beg_date_taken_from_user < $beg_date1 && $end_date_taken_from_user < $beg_date1) {

                        $this->reservationCreate($room_id, $reserver, $description, $beg_date_taken_from_user, $end_date_taken_from_user);

                        return $this->setStatusCode(200)->respond(['message' => 'Резервация добавлена']);

                    } elseif ($beg_date_taken_from_user > $beg_date1 && $beg_date_taken_from_user > $end_date1) {

                        $this->reservationCreate($room_id, $reserver, $description, $beg_date_taken_from_user, $end_date_taken_from_user);

                        return $this->setStatusCode(200)->respond(['message' => 'Резервация добавлена']);

                    } else {
                        return $this->setStatusCode(400)->respond(['message' => 'Пересекающие даты']);
                    }
                }
            }

            $this->reservationCreate($room_id, $reserver, $description, $beg_date_taken_from_user, $end_date_taken_from_user);

            return $this->setStatusCode(200)->respond(['message' => 'Резервация добавлена']);
        }
    }

    public function reservationCreate($room_id, $reserver, $description, $beg_date, $end_date)
    {
        Reservation::create([
            'room_id' => $room_id,
            'who_reserved_the_room' => $reserver,
            'description' => $description,
            'beginning_date' => $beg_date,
            'ending_date' => $end_date,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'who_reserved_the_room' => 'required|max:255',
            'description' => 'required|max:255',
            'beginning_date' => 'required|date',
            'ending_date' => 'required|date'
        ]);
        $beg_date_taken_from_user = Carbon::create($request->input('beginning_date'))->format('Y-m-d H:i:s');
        $end_date_taken_from_user = Carbon::create($request->input('ending_date'))->format('Y-m-d H:i:s');
        $now = Carbon::now();
        if ($beg_date_taken_from_user < $now && $end_date_taken_from_user < $now) return $this->setStatusCode(400)->respond(['message' => 'Дата уже прошла']);
        $room = Room::find($request->input('room_id'));
        $reserve = Reservation::find($id);
        foreach ($room->reservations as $reservation) {
            if (count($room->reservations) >= 2 && $reservation->id !== $reserve->id) {
                $beg_date1 = Carbon::create($reservation->beginning_date)->format('Y-m-d H:i:s');
                $end_date1 = Carbon::create($reservation->ending_date)->format('Y-m-d H:i:s');
                if ($beg_date_taken_from_user < $beg_date1 && $end_date_taken_from_user < $beg_date1 || $beg_date_taken_from_user > $beg_date1 && $beg_date_taken_from_user > $end_date1) {
                    $reserve->update([
                        'who_reserved_the_room' => $request->input('who_reserved_the_room'),
                        'description' => $request->input('description'),
                        'beginning_date' => $beg_date_taken_from_user,
                        'ending_date' => $end_date_taken_from_user,
                    ]);
                    return $this->setStatusCode(200)->respond(['message' => 'Резервация обновлена']);
                } else {
                    return $this->setStatusCode(400)->respond(['message' => 'Пересекающие даты']);
                }
            } else if (count($room->reservations) == 1) {
                $reserve->update([
                    'who_reserved_the_room' => $request->input('who_reserved_the_room'),
                    'description' => $request->input('description'),
                    'beginning_date' => $beg_date_taken_from_user,
                    'ending_date' => $end_date_taken_from_user,
                ]);
                return $this->setStatusCode(200)->respond(['message' => 'Резервация обновлена']);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd($id);
        Reservation::find($id)->delete();
//        $reservation->delete();
    }
}
