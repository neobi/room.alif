<?php

namespace App\Jobs;

use App\Reservation;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckReservationExpirationDate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reservations = Reservation::all();
        $now = Carbon::now();
        foreach ($reservations as $reservation) {
            $ending_date = Carbon::create($reservation->ending_date);
            if ($now > $ending_date) {
                $reservation->delete();
            }
        }
    }
}
