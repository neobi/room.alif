import Settings from './components/Settings.vue';
import Home from './components/Home.vue';


export default [
    {
        path: "/",
        component: Home,
        name: "home",
    },
    {
        path: "/settings",
        component: Settings,
        name: "settings",
    },
    // {
    //     path: "/settings",
    //     component: Settings,
    //     name: "settings",
    // }
]


