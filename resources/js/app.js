
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.css";
import VueRouter from "vue-router";
import routes from "./routes";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import Swal from 'sweetalert2';

// import moment from 'moment';

window.moment = require('moment');

// require("moment/min/locales.min");
moment.locale('ru');
// window.moment.tz.setDefault("Asia/Dushanbe");

require('./bootstrap');


window.Vue = require('vue');
Vue.use(VueRouter);

export const EventBus = new Vue();

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('app', require('./App.vue').default);
Vue.component('home', require('./components/Home.vue').default);
Vue.component('navbar', require('./components/Navbar.vue').default);
Vue.component('show-reservation-info-modal', require('./components/Modals/ShowReservationInfoModal.vue').default);
Vue.component('settings', require('./components/Settings.vue').default);
Vue.component('reservation-modal', require('./components/Modals/ReservationModal.vue').default);
Vue.component('update-room-modal', require('./components/Modals/UpdateRoomModal.vue').default);
Vue.component('show-room-reservations', require('./components/ShowRoomReservations.vue').default);
Vue.component('update-reservation-modal', require('./components/Modals/UpdateReservationModel.vue').default);
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);


const router = new VueRouter({
    mode: 'history',
    routes,

});

const app = new Vue({
    router,
    el: '#app',
});
