<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/settings', 'HomeController@index')->name('settings');
Route::resource('/rooms', 'RoomController');
Route::get('/room/reservations/show/{id}', 'RoomController@showReservationsOfRoom');
